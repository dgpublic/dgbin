# Configuring the dgbin project
#
# 30 Jun, Author: Yakov Nae, yakovnae@dspgeo.com
# This source code is subject to DSPGeo Permissive License. Please 
#  see license.txt file for more information. All rights reserved.
# Tel: +55(19)3521-4998 Campinas, SP-Brazil; http://www.dspgeo.com
dgbinstart(){
SCRIPT_PATH=$(cd `dirname "${BASH_SOURCE[0]}"` && pwd)
export DGBIN=$SCRIPT_PATH
source $DGBIN/config.sh init
#TODO put a message that dgbin is on here 
}
export -f dgbinstart