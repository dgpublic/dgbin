# Configuring the dgbin project
#
# 14 Mar, Author: Yakov Nae, yakovnae@dspgeo.com
# This source code is subject to DSPGeo Permissive License. Please 
#  see license.txt file for more information. All rights reserved.
# Tel: +55(19)3521-4998 Campinas, SP-Brazil; http://www.dspgeo.com
DG_CLEAN=0
DG_INIT=0
if [[ -z "$DGBIN" ]]; then echo -e "\n\nERROR: DGBIN was not set, see ReadMe for more information\n\n"; exit; fi
for i; do
  [[ $i = init  ]] && DG_INIT=1
  [[ $i = clean ]] && DG_CLEAN=1
done
if [[ $DG_CLEAN = 1 ]]; then
  echo cleaning the dgbin project...
  rm -rf $DGBIN/trash
  rm -f $DGBIN/*~
  rm -f $DGBIN/bin/*~
fi
add_fun(){
  source $DGBIN/bin/$1.sh
  export -f $1
}
if [[ $DG_INIT = 1 ]]; then
  mkdir -p $DGBIN/trash
  
  add_fun dgbinhelp

  add_fun dgimage
  add_fun dgplot
  add_fun dgprepar
fi

