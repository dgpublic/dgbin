# dgbinhelp - Help for the DGBIN package
#
# 20 Mar 2015, Author: Yakov Nae, yakovnae@dspgeo.com
# This source code is subject to DSPGeo Permissive License. Please 
#  see license.txt file for more information. All rights reserved.
# Tel: +55(19)3521-4998 Campinas, SP-Brazil; http://www.dspgeo.com

dgbinhelp(){
local TEMP_PWD=$(pwd)
cd $DGBIN/bin
rm *~
clear
echo "DGBIN - DSPGeo package for SGY files that uses Seismic Unix:"
for i in *
do
  line=$(head -n 1 $i)
  line=${line:1}
  echo $line | { read first second rest ; printf " %-15s - %s\n" $first "$rest"; }
#  echo $line | { read first second rest ; echo -e "$first \t $rest"; }
done
cd $TEMP_PWD
}
