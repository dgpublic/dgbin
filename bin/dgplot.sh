# dgplot - Visualization tool for the dspgeostack processing program
#
#TODO if a volum with sgy ext is entered - dont mess with it:
#   args=( "md25" "md50" "vemodel.sgy" )
#
# 14 Mar 2016, Author: Yakov Nae, yakovnae@dspgeo.com
# This source code is subject to DSPGeo Permissive License. Please 
#  see license.txt file for more information. All rights reserved.
# Tel: +55(19)3521-4998 Campinas, SP-Brazil; http://www.dspgeo.com

dgplot(){
  dgplot_main(){
    local OUTNAME=$1
    local ATT=$2
    TEMP_FLDR=$(pwd)
    local DGTRASH=$DGBIN/trash
    mkdir -p $DGTRASH

    declare -a pre_set_stack=( "filter" "agc=1" "perc=99" "cmap=rgb1" "ybox=10" "key=cdp" )
    declare -a pre_set_att=( "legend=1" "cmap=hsv2" "ybox=10" )
    local FOLDER=$(pwd)
    local n_st=${#pre_set_stack[@]}
    local n_at=${#pre_set_att[@]}
    local count=1
    for i; do
      case $i in
        "path="*)    FOLDER=${i:5}        ;;
        *) if [[ $count > 2 ]]; then 
             pre_set_stack[$n_st]="$i"; n_st=$(( $n_st+1 )); 
             pre_set_att[$n_at]="$i"; n_at=$(( $n_at+1 )); 
           fi ;;
      esac
      count=$(( $count +1 ))
    done

    [[ "$#" < 2 ]] && dgplot_show_help
    cd $DGTRASH

    if [[ $ATT = 'all' ]]; then
      dgimage $FOLDER/$OUTNAME.stack.sgy "${pre_set_stack[@]}"
      dgimage $FOLDER/$OUTNAME.coher.sgy "${pre_set_att[@]}" 
      dgimage $FOLDER/$OUTNAME.vstk.sgy  "${pre_set_att[@]}" 
      dgimage $FOLDER/$OUTNAME.dip.sgy   "${pre_set_att[@]}" 
      dgimage $FOLDER/$OUTNAME.curv.sgy   "${pre_set_att[@]}" 
    else
      eval 'declare -a MD=($OUTNAME)'
      for (( ii=0; $ii<${#MD[@]}; ii+=1 ));
      do
         unset DG_FILENAME
         [[ -f $FOLDER/${MD[$ii]}.sgy ]]    && local DG_FILENAME=$FOLDER/${MD[$ii]}.sgy
         [[ -f $FOLDER/${MD[$ii]}.segy ]]   && local DG_FILENAME=$FOLDER/${MD[$ii]}.segy
         [[ -f $FOLDER/${MD[$ii]}.$ATT.sgy ]] && local DG_FILENAME=$FOLDER/${MD[$ii]}.$ATT.sgy
         if [[ -z "$DG_FILENAME" ]]; then echo "ERROR: file $FOLDER/${MD[$ii]}* does not exist"; cd $TEMP_FLDR; return; fi
         if [[ $ATT = 'stack' ]]; then
           dgimage $DG_FILENAME "${pre_set_stack[@]}"
         fi
         if [[ $ATT = 'coher' ]] || [[ $ATT = 'vstk' ]] || [[ $ATT = 'dip' ]] || [[ $ATT = 'curv' ]] ; then 
           dgimage $DG_FILENAME "${pre_set_att[@]}"
         fi
      done
    fi #if [[ $ATT = 'all' ]];
    cd $TEMP_FLDR
  } #dgplot_main

  dgplot_show_help(){

    local h=""
    h+="DGPLOT - Visualization tool for the dspgeostack processing program.\n"
    h+=" This function can either display a single configuration or\n" 
    h+=" cross compare an attribute through a list of configurations\n"
    h+="\n" 
    h+=" SYNTAX:"
    h+=" dgplot "outname" <mode> [optional parameters]\n"
    h+=" <mode>: all,stack,coher,dip,vstk\n"
    h+=" outname: Need to be between quotes, result prefix that was feed to dspgeo par file\n"
    h+=" optionals:\n"
    h+="  path=/path/to/res - will read files from this path, else from current path\n"
    h+="  tmax=t2           - will display up to t2\n"
    h+="  tmin=t1           - will display from t1\n"
    h+="  (*) all suximage and dgimage options\n"
    h+="\n"
    h+="\n"
    h+="\n"
    h+=" EXAMPLES:\n"
    h+=" $ dgplot "cnf" all\n"
    h+=" display the following volumes in the current folder:\n"
    h+="   cnf.stack, cnf.coher, cnf.dip, cnf.vstk\n" 
    h+="\n" 
    h+=" $ dgplot \"cnf1 cnf2 cnf3\" stack\n"
    h+=" display the following volumes in the current folder:\n"
    h+="   cnf1.stack, cnf2.stack, cnf3.stack\n" 
    h+="\n"
    h+=" $ dgplot \"cnf1 cnf2 cnf3\" coher clip=0.5\n"
    h+=" display the following volumes while clipping amplitude 0.5:\n"
    h+="   cnf1.coher, cnf2.coher, cnf3.coher\n" 
    h+="\n"
    h+=" $ dgplot \"cnf1 cnf2 cnf3\" stack path=/path/to/results\n"
    h+=" display the following volumes in /path/to/results:\n"
    h+="   cnf1.stack, cnf2.stack, cnf3.stack\n" 
    h+="\n"
    h+=" Presets:\n"
    h+=" The following presets can be over-rided by the user:\n"
    h+=" (*) stack preset: ${pre_set_stack[@]}\n"
    h+=" (*) attribute preset: ${pre_set_att[@]}\n"
    h+="\n"
    
    clear
    echo -e "$h" | more
  }

  dgplot_main  "$@"
} 
