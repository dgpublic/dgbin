# dgimage - SEGY and SU visualization based on suximage
#  This is the very beginning of our graphic interface
#
# 22 Nov 2015, Author: Yakov Nae, yakovnae@dspgeo.com
# This source code is subject to DSPGeo Permissive License. Please 
#  see license.txt file for more information. All rights reserved.
# Tel: +55(19)3521-4998 Campinas, SP-Brazil; http://www.dspgeo.com

dgimage()
{
  dgimage_main(){
  if [[ $# = 0 ]]; then dgimage_help; return; fi
  if [[ ! -f $1 ]]; then echo "Error: input file does not exist"; return; fi
  local DGTRASH=$DGBIN/trash
  mkdir -p $DGTRASH
  TITLE=$(basename "$1")
  TITLE="title=${TITLE%.*}"
  declare -a args=()
  local PRESET_FILTER="f=10,20,40,50"
  local FLTR=0; 
  local AGC=0; 
  local WIGB=0; 
  local TMIN=0; 
  local TMAX=0; 
  local CDPMIN=-1; 
  local CDPMAX=-1;
  local OFFMIN=-1; 
  local OFFMAX=-1;
  local VERBOSE=0;

  ############################
  ###       PARSING        ###
  ############################
  index=0;
  for i; do
    case $i in
       "ps=seis")  AGC="agc=1"; FLTR=$PRESET_FILTER; 
                   args[$index]='cmap=rgb1'; index=$(( $index+1 ));
                   args[$index]='perc=99'; index=$(( $index+1 ));;
        "ps=att")  args[$index]='legend=1'; index=$(( $index+1 ));
                   args[$index]='cmap=hsv1'; index=$(( $index+1 ));;                  
        "wigb=1")  WIGB=1              ;;
        "tmin="*)  TMIN=$i             ;;
        "tmax="*)  TMAX=$i             ;;
         "agc="*)  AGC=$i              ;;
      "filter=1")  FLTR=$PRESET_FILTER ;;
      "filter=0")  FLTR=0              ;;
      "filter="*)  FLTR="f="${i:7}     ;;
       "title="*)  TITLE=$i            ;;
      "cdpmin="*)  CDPMIN=${i:7}       ;;
      "cdpmax="*)  CDPMAX=${i:7}       ;;
      "offmin="*)  OFFMIN=${i:7}       ;;
      "offmax="*)  OFFMAX=${i:7}       ;;
     "verbose=1")  VERBOSE=1           ;;
      *) args[$index]=$i; index=$(( $index+1 )) ;;
    esac
  done
  ############################
  ###      CHECK I/O       ###
  ############################
  if [[ ! $AGC = "agc=1" ]] && [[ ! $AGC = "agc=0" ]] && [[ ! $AGC = 0 ]]; then echo "Error: agc should be 0 or 1"; return; fi

  mkdir -p $DGTRASH
  local EXTENSION=`echo "$1" | cut -d'.' -f2`
  if [[ $EXTENSION = 'su' ]]; then 
    cp $1 $DGTRASH/tmp.su;
  else 
    segyread tape=$1 hfile=$DGTRASH/h bfile=$DGTRASH/b | segyclean >$DGTRASH/tmp.su
  fi

  if [[ $VERBOSE = 1 ]]; then echo; echo "Processing"; fi 
  if [[ ! $TMAX = 0 ]] ; then 
    suwind <$DGTRASH/tmp.su $TMAX >$DGTRASH/tmp1.su
    if [[ ! -f $DGTRASH/tmp1.su ]]; then echo "Error tmax wind"; return; fi
    if [[ $VERBOSE = 1 ]]; then echo "->tmax window"; fi 
    mv $DGTRASH/tmp1.su $DGTRASH/tmp.su
  fi
  if [[ ! $TMIN = 0 ]] ; then 
    suwind <$DGTRASH/tmp.su $TMIN >$DGTRASH/tmp1.su
    if [[ ! -f $DGTRASH/tmp1.su ]]; then echo "Error tmin wind"; return; fi
    if [[ $VERBOSE = 1 ]]; then echo "->tmin window"; fi 
    mv $DGTRASH/tmp1.su $DGTRASH/tmp.su
  fi
  if [[ ! $CDPMIN = -1 ]] ; then 
    local cdpmax="$(surange <$DGTRASH/tmp.su key=cdp | awk 'NR==2{print $3;}')"
    suwind <$DGTRASH/tmp.su key=cdp min=$CDPMIN max=$cdpmax >$DGTRASH/tmp1.su
    if [[ ! -f $DGTRASH/tmp1.su ]]; then echo "Error cdpmin wind"; return; fi
    if [[ $VERBOSE = 1 ]]; then echo "->cdpmin window"; fi 
    mv $DGTRASH/tmp1.su $DGTRASH/tmp.su
  fi
  if [[ ! $CDPMAX = -1 ]] ; then 
    local cdpmin="$(surange <$DGTRASH/tmp.su key=cdp | awk 'NR==2{print $2;}')"
    suwind <$DGTRASH/tmp.su key=cdp min=$cdpmin max=$CDPMAX >$DGTRASH/tmp1.su
    if [[ ! -f $DGTRASH/tmp1.su ]]; then echo "Error cdpmax wind"; return; fi
    if [[ $VERBOSE = 1 ]]; then echo "->cdpmax window"; fi 
    mv $DGTRASH/tmp1.su $DGTRASH/tmp.su
  fi
  if [[ ! $OFFMIN = -1 ]] ; then 
    local offmax="$(surange <$DGTRASH/tmp.su key=offset | awk 'NR==2{print $3;}')"
    suwind <$DGTRASH/tmp.su key=offset min=$OFFMIN max=$offmax >$DGTRASH/tmp1.su
    if [[ ! -f $DGTRASH/tmp1.su ]]; then echo "Error offmin wind"; return; fi
    if [[ $VERBOSE = 1 ]]; then echo "->offmin window"; fi
    mv $DGTRASH/tmp1.su $DGTRASH/tmp.su
  fi
  if [[ ! $OFFMAX = -1 ]] ; then 
    local offmin="$(surange <$DGTRASH/tmp.su key=offset | awk 'NR==2{print $2;}')"
    suwind <$DGTRASH/tmp.su key=offset min=$offmin max=$OFFMAX >$DGTRASH/tmp1.su
    if [[ ! -f $DGTRASH/tmp1.su ]]; then echo "Error offmax wind"; return; fi
    if [[ $VERBOSE = 1 ]]; then echo "->offmax window"; fi
    mv $DGTRASH/tmp1.su $DGTRASH/tmp.su
  fi
  if [[ ! $AGC = 0 ]] ; then 
    sugain <$DGTRASH/tmp.su $AGC >$DGTRASH/tmp1.su
    if [[ ! -f $DGTRASH/tmp1.su ]]; then echo "Error agc gain"; return; fi
    if [[ $VERBOSE = 1 ]]; then echo "->agc gain"; fi
    mv $DGTRASH/tmp1.su $DGTRASH/tmp.su
  fi
  if [[ ! $FLTR = 0 ]] ; then 
    sufilter <$DGTRASH/tmp.su $FLTR >$DGTRASH/tmp1.su
    if [[ ! -f $DGTRASH/tmp1.su ]]; then echo "Error filter"; return; fi
    if [[ $VERBOSE = 1 ]]; then echo "->filter"; fi
    mv $DGTRASH/tmp1.su $DGTRASH/tmp.su
  fi
  if [[ $VERBOSE = 1 ]]; then echo; fi 
  
  if [[ $WIGB = 1 ]] ; then
    suxwigb  <$DGTRASH/tmp.su "${args[@]}" $TITLE &
  else
    suximage <$DGTRASH/tmp.su "${args[@]}" $TITLE &
    echo $!
  fi
  #TODO test <wait> instrad of sleep 
  sleep 0.01
  rm -f $DGTRASH/h 
  rm -f $DGTRASH/b
  rm -f $DGTRASH/tmp.su
  rm -f $DGTRASH/tmp1.su
}
  dgimage_help(){
    clear 
    local h=""
    h+="DGIMAGE - Plot of SEGY data files (using suximage)\n"
    h+=" SYNTAX:"
    h+=" dgimage [data.sgy] [optional parameters]\n"
    h+=" example:\n"
    h+=" dgimage data.sgy\n"
    h+=" Optional Parameters:\n"
    h+="   ps=none    \t- Presets: ps=seis for seismic display ps=att for attributes\n"
    h+="   wigb=$WIGB \t- Display in wiggle mode if turned on\n"
    h+="   tmin=$TMIN \t- tmin value for wind (if on)\n"
    h+="   tmax=$TMAX \t- tmax value for wind (if on)\n"
    h+="   agc=$AGC   \t- agc value for gain (turn off by agc=0)\n"
    h+=" title=filename - title, if not defined is set to the file name\n"
    h+="cdpmin=all \t- min value for cdp\n"
    h+="cdpmax=all \t- max value for cdp\n"
    h+="offmin=all \t- min value for cdp\n"
    h+="offmax=all \t- max value for cdp\n"
    h+="filter=$FLTR  \t- activates sufilter (filter=1 gives the preset $PRESET_FILTER\n"
    h+="                                     (change filter by e.g. filter=1,10,20,30)\n"
    h+="verbose=0 \t- verbose=1 prints all process before plot\n"
    h+="The following options from suximage are also functional:\n"
    h+="  perc, cmap, legend, xbox, ybox, hbox, wbox\n"
    clear
    echo -e "$h" | more
  }
  dgimage_main "$@"
}
