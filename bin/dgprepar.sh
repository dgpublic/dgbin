# dgprepar - Creates .par files for dspgeostack execution
#
# 31 Mar 2016, Author: Joao Ralha, ralha@dspgeo.com
# This source code is subject to DSPGeo Permissive License. Please 
#  see license.txt file for more information. All rights reserved.
# Tel: +55(19)3521-4998 Campinas, SP-Brazil; http://www.dspgeo.com

dgprepar(){
dgprepar_main(){
  local DGTRASH=$DGBIN/trash
  mkdir -p $DGTRASH

  if [[ "$#" = 0 ]]; then dgprepar_help; return; fi
  local tmpPWD=$(pwd)
  local PAR=""
  local PREFIX=""

  local OUTPUT=""
  local VAR1=""
  local VAR2=""  
  local ARG1=" " 
  local ARG2=" "
  local INFILE=""
  local ext=""
  local answer="n"
  
  ############################
  ###       PARSING        ###
  ############################
  local index=0
  local i
  for i; do
    case $i in

         *".par") INFILE=$i ;;
         *".sgy") INFILE=$i ;;
        *".segy") INFILE=$i ;;
      "prefix="*) PREFIX=${i:7} ;; #pre-fix for the configuration (e.g. pre_md100off1000)z
           *"="*) if [[ $index = 0 ]]; then ARG1=$(echo $i | cut -d "=" -f2); VAR1="${i%=*}"; fi #ARG before '=' VAR after '='
                  if [[ $index = 1 ]]; then ARG2=$(echo $i | cut -d "=" -f2); VAR2="${i%=*}"; fi #ARG before '=' VAR after '='
                  local index=$(( $index+1 )) ;;
    esac
  done

  ext=$(echo "$INFILE" | cut -d'.' -f2)
  if [[ $ext == "par" ]]; then PAR=$INFILE;   fi
  if [[ $ext == "sgy" ]] || [[ $ext == "segy" ]]; then
    if [[ ! -e $INFILE ]]; then echo -n "Pre-stack file doesn't exist, are you sure you want to proceed?(y/n)"; read answer;
      if [[ ! $answer == "y" ]] && [[ ! $answer == "Y" ]]; then return; fi
    fi
    create_par_file $DGTRASH/cnf.par $INFILE; PAR=$DGTRASH/cnf.par;
  fi

  ############################
  ###      CHECK I/O       ###
  ############################
  if [[ $INFILE = "" ]]; then echo "Error: Input file is neither a .par or .sgy file."; return; fi
  if [[ $index >2         ]]; then echo "error only 2 variation parameters allowed"; return; fi
  if [[ $PREFIX = "./"*   ]]; then PREFIX=$tmpPWD/${PREFIX:2};  fi #Relative path
  if [[ ! $PREFIX = "/"*  ]]; then PREFIX=$tmpPWD/$PREFIX;      fi #File isn't relative or absolute
  if [[ $ARG1 = " "  ]] && [[ $PREFIX = *"/" ]]; then PREFIX+=cnf; fi #Case no var1 no var2
  if [[ ! $PAR = "" ]] && [[ ! -e $PAR ]]; then echo "Error: Reference par file doesn't exist"; return; fi

  ############################
  # LOOP OVER CONFIGURATIONS #
  ############################
  local IFS="," #Used to separate ARG! and ARG2 with commas
  for i1 in $ARG1; do
    for i2 in $ARG2; do
      local CNF="$PREFIX$VAR1$i1$VAR2$i2" #Configuration name
      CNF=${CNF/  /}; CNF=${CNF/ /}       #Removing white spaces (case var1 var2 are not defined)
      cp $PAR $CNF.par

      par_vary_parameter $CNF.par outname $CNF #changing outname
      par_vary_parameter $CNF.par $VAR1 $i1    #changing VAR1
      par_vary_parameter $CNF.par $VAR2 $i2    #changing VAR2
    done
  done


rm -rf $DGTRASH/cnf.par
}

create_par_file(){
  local PAR=$1
  local INPUT=$2

cat > $PAR <<EOF
input $INPUT
outname $PAR
vsurf 3000
mdmax1 300
mdmax2 300
tmark1 0.1
tmark2 1
offmax1 3000
offmax2 3000
EOF
}

par_vary_parameter()
{
  local PAR_FILE=$1
  local PARAMETER=$2
  local NEW_VALUE=$3
  if [[ $PARAMETER = " " ]]; then return; fi
  if [[ $PARAMETER == mdmax ]]; then
    vary_parameter $PAR_FILE mdmax1 $NEW_VALUE
    vary_parameter $PAR_FILE mdmax2 $NEW_VALUE
    return
  fi
  if [[ $PARAMETER == offmax ]]; then
    vary_parameter $PAR_FILE offmax1 $NEW_VALUE
    vary_parameter $PAR_FILE offmax2 $NEW_VALUE
    return
  fi
  vary_parameter $PAR_FILE $PARAMETER $NEW_VALUE
}

vary_parameter()
{
  #This function changes a pre-defined parameter on a *.par file
  local PAR_FILE=$1
  local PARAMETER=$2
  local NEW_VALUE=$3
  local parameters="$(grep -Eo '^[^ ]+' $PAR_FILE)" #first word at each line
  parameters=$(echo $parameters |  while read line; do echo -n "$line "; done)

  local num=$(awk -v a="$parameters" -v b="$PARAMETER" 'BEGIN{print index(a,b)}')
  num=$(( $(grep -o " " <<< ${parameters::$num} | wc -l) ))
echo $num
  if [ $num = 0 ]; then 
    echo "$PARAMETER $NEW_VALUE" >>$PAR_FILE
  else 
    < $PAR_FILE head -n +"$num" >head.par
    num=$(( $num + 2 ))
    < $PAR_FILE tail -n +"$num" >tail.par
    echo "$PARAMETER $NEW_VALUE" >>head.par
    cat tail.par >> head.par
    mv head.par $PAR_FILE
    rm tail.par
  fi
}

dgprepar_help(){
    clear
    local h=""
    h+="DGPREPAR. - Modifies or generates *.par files for execution.\n" 
    h+=" SYNTAX: \n"
    h+="   dgprepar input=/path/to/sgy [optionals]\n"
    h+="   dgprepar par=cnf.par        [optionals]\n\n"
    h+=" Optional Parameters: \n"
    h+="       input=  valid path to pre-stack input file\n"
    h+="         par=  valid path to a reference *.par file\n"
    h+="      prefix=  prefix for outname (can contain fullpath)\n";
    h+="  var1=,var2=  up to 2 variables to vary\n\n"
    h+=" Examples: \n"
    h+="   dgprepar mdmax=300,400 offmax=500,600 par=example.par\n"
    h+="   -> creates configurations:\n"
    h+="   mdmax300offmax500 mdmax300offmax600 mdmax400offmax500 mdmax400offmax600\n"
    h+="   where mdmax1=mdmax2 and offmax1=offmax2\n"
    echo -e "$h" | more
}
dgprepar_main $@
}
